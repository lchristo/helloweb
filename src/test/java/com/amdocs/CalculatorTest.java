package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;

public class CalculatorTest {
    @Test
    public void testAdd() throws Exception {

        int k= new Calculator().add(3,6);
        assertEquals("Add", 9, k);

    }
   @Test
    public void testSub() throws Exception {

        int k= new Calculator().sub(6,3);
        assertEquals("Sub", 3, k);

    }
}

